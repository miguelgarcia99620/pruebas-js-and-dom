"use strict";

function main() {
  // Genera una contraseña (número entero aleatorio del 0 al 100)
  let password = Math.floor(Math.random() * 101);
  // Testeo console.log(password);
  // El usuario tendrá 5 oportunidades de acertar
  let lives = 5;
  let guess = -1;
  alert("El juego ha comenzado. Tiene 5 oportunidades para acertar el número.");
  while (lives != 0 && password != guess) {
    // Pide al usuario que introduzca un número dentro del rango
    do {
      guess = prompt("Introduzca un número dentro del rango (0-100).");
    } while (guess < 0 || guess > 100);
    // Se indica si la contraseña es un número mayor o menor al introducido
    if (password > guess) {
      alert("La contraseña es un número mayor al introducido.");
    }
    if (password < guess) {
      alert("La contraseña es un número menor al introducido.");
    }
    lives--;
  }
  if (password == guess) {
    // Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado
    alert("¡Ganaste!");
  } else {
    // Si no lo consigue, aparecerá un mensaje indicando que ha perdido
    alert("¡Perdiste!");
  }
}

main();
