"use strict";

// Puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

// Edita el archivo script.js para crear una función que reciba un array de objetos (equipos y puntos conseguidos)
// y muestre por consola el ** nombre ** del que más y del que menos puntos hayan conseguido, con sus respectivos ** totales **.

function getPoints(serie) {
  let team = [];
  let points = [];
  const reducer = (previousValue, currentValue) => previousValue + currentValue;

  for (let i = 0; i < serie.length; i++) {
    team.push(serie[i].equipo);
    let total = Object.values(serie[i].puntos);
    points.push(total.reduce(reducer));
  }

  let max = points.indexOf(Math.max(...points));
  let min = points.indexOf(Math.min(...points));

  console.log(`El equipo que más puntos ha conseguido ha sido ** ${team[max]} ** con 
  ** ${points[max]} ** puntos y el que menos ha sido ** ${team[min]} ** con ${points[min]} puntos.`);
}

getPoints(puntuaciones);
